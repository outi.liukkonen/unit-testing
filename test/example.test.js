const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {

    let myvar = undefined;
    before(() => {
        myvar = 1;
    })
    it('Should return 2 when using some funktion with a=1, b=1', () => {
        const result = mylib.sum(1,1)
        expect(result).to.equal(2)
    })
    it.skip('Assert foo in not bar', () => {
        assert('foo' !== 'bar');
    })
    it('myvar should exist', () => {
        should.exist(myvar)
    })
    after(() => {
        console.log('hello')
    })
})